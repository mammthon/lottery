package svc

import (
	"context"
	"time"
)

type Service struct {
}

func (s *Service) QueryList(ctx context.Context, stime time.Time, etime time.Time) {

}

func (s *Service) QueryListByGameType(ctx context.Context, gameType int, stime time.Time, etime time.Time) {

}

func (s *Service) QueryBalance(ctx context.Context, userID int) {

}

func (s *Service) SaveLottery(ctx context.Context, gids []int) (orderID int, ticketIDs []int, err error) {
	return 0, nil, err
}

func (s *Service) Calculate(ctx context.Context, gids []int, base int) (ans int, err error) {
	gids = []int{1, 2, 3, 4}
	ans = base
	for _, arr := range gids {
		// check one is invalid
		ans = ans * arr
	}
	return ans, nil
}

func (s *Service) verify() {

}

func (s *Service) login() {

}
