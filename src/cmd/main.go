package main

import (
	"context"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	cron "github.com/go-co-op/gocron"
	"github.com/go-redis/redis/v8"
	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v8"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	Port       string
	Version    string
	CommitHash string
	BuildTime  string
	NameSpace  string
)

const (
	_GetAgeQuerySQL = "SELECT * FROM student WHERE name=\"joe\""
)

func main() {
	s := cron.NewScheduler(time.UTC)
	s.Every(5).Seconds().Do(func() { fmt.Printf("Every 5 second running, ticker at %v\n", time.Now().Format(time.RFC3339)) })

	ctx := context.Background()
	client := redis.NewClient(&redis.Options{
		Addr:     "20.89.107.19:6379",
		Password: "P7U89@EJN4",
		DB:       0, // use default DB
	})
	pool := goredis.NewPool(client)
	rs := redsync.New(pool)
	mux := rs.NewMutex("mammthon-distributed-mutex")

	// Obtain a lock for our given mutex. After this is successful, no one else
	// can obtain the same lock (the same mutex name) until we unlock it.
	if err := mux.Lock(); err != nil {
		panic(err)
	}
	// Do your work that requires the lock.
	// Release the lock so other processes or threads can obtain a lock.
	if ok, err := mux.Unlock(); !ok || err != nil {
		panic("unlock failed")
	}

	err := client.Set(ctx, "name", "trump", 1000*time.Second).Err()
	if err != nil {
		panic(err)
	}

	val, err := client.Get(ctx, "name").Result()
	if err != nil {
		panic(err)
	}
	fmt.Printf("\nkey is %v\n", val)

	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
	dsn := "root:P7U89@EJN4@tcp(20.89.107.19:3306)/mammthon?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Printf("err is %v\n", err)
	}
	db.Exec(_GetAgeQuerySQL)
	fmt.Println(db.Name())

	fmt.Printf("Port: %v\t Version:%v\t CommitHash:%v\t BuildTime:%v\t NameSpace:%v \n", Port, Version, CommitHash, BuildTime, NameSpace)

	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	port := fmt.Sprintf(":%v", Port)
	s.StartAsync()
	r.Run(port)
}
