package model

import (
	"time"
)

// Team 就是比赛队伍
type Team struct {
	ID    int
	Name  string
	Desc  string
	ctime time.Time
	mtime time.Time
}

// Lottery 就是彩种,比如混合过关 胜平负...
type Lottery struct {
	ID    int
	Name  string
	Desc  string
	ctime time.Time
	mtime time.Time
}

// Gamble 就是具体赌注赔率
type Gamble struct {
	ID          int
	GameID      int    // 足球 篮球
	MatchID     int    // 比赛ID
	LotteryType int    // 彩种类型
	LotteryRate int    // 赔率
	LotteryData string // 赌注数据
	BuyState    int    // 购买状态 0 尚未开始 1 正常下注 2 不能下注
	State       int    // 0 尚未分出胜负 1 押对了 2 押错了 3 取消
	ctime       time.Time
	mtime       time.Time
}

// Games 就是比赛类型 足球 篮球
type Games struct {
	ID    int
	Name  string
	Desc  string
	ctime time.Time
	mtime time.Time
}

type Match struct {
	GameID      int
	HomeTeamID  int
	HomeScore   int
	GuestTeamID int
	GuestScore  int
	MatchTime   time.Time
	Location    string
	State       int // 0 未开始 1 进行中 2 暂停 3 比赛结束
	ctime       time.Time
	mtime       time.Time
}

// Ticket 就是彩票订单
type Ticket struct {
	ID     int
	Gids   string `json:"gids"`               // '英文逗号分割'
	Counts int    `json:"counts" default:"1"` // '倍数' 默认1
	ctime  time.Time
	mtime  time.Time
}

type Order struct {
	ID      int
	UserID  int
	Tickets string
	ctime   time.Time
	mtime   time.Time
}

type Wallet struct {
	ID       int
	UserID   int
	Balance  int
	Currency string // default 角
	ctime    time.Time
	mtime    time.Time
}

type User struct {
	ID    int
	Paid  int
	Name  string
	Phone string
	State int //  0 正常 1 封禁 2 主动注销
	ctime time.Time
	mtime time.Time
}
