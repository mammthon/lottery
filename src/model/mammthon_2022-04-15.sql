drop table IF EXISTS student;
CREATE TABLE IF NOT EXISTS student (
    `id` bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `age` int NOT NULL DEFAULT 0,
    `name` varchar(64) NOT NULL DEFAULT '',
    `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
    `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;