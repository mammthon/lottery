output=main
namespace="Lottery"
tag=`git describe --tag  --abbrev=0`
build_time=`date +%FT%T%z`
commit_hash=`git rev-parse --short HEAD`
port=10000

# .PHONY: build 代表每次都可以remake, 出处：http://www.gnu.org/software/make/manual/html_node/Phony-Targets.html
.PHONY: build
build: dep
	go build -tags dynamic -ldflags "-X main.NameSpace=${namespace} \
			-X main.Version=${tag} \
			-X main.CommitHash=${commit_hash} \
			-X main.Port=${port} \
			-X main.BuildTime=${build_time} " \
			-race \
			-o ./build/app \
			./src/cmd

.PHONY: run
run: build
	@./build/app

kill-proc:
	lsof -i :${port} | awk '$$1 ~ /app/ { print $$2 }' | xargs kill -9 || true


# 格式化 import 分组 和 代码格式化
format:
	$(shell go env GOPATH)/bin/gofumpt -w .
	$(shell go env GOPATH)/bin/gci -w .
	$(shell go env GOPATH)/bin/goimports -w .

dep:
	go mod tidy -compat=1.17

setup:	
	go install -v golang.org/x/tools/cmd/goimports@latest
	go install -v github.com/daixiang0/gci@latest
	go install -v github.com/swaggo/swag/cmd/swag@latest


.PHONY: golangci-install
golangci-install:
	@brew install golangci-lint

.PHONY: golangci
golangci:
	@go list -e -compiled -test=true -export=false -deps=true -find=false -tags=dynamic -- ./... > /dev/null
	@golangci-lint run ./...


.PHONY: pre-hook-init
pre-hook-init:
	@brew install pre-commit
	@pip3 install pre-commit
	@pre-commit install
	@pre-commit install --hook-type commit-msg
